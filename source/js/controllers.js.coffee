@zappdobara.controller 'MainCtrl', ($scope, $authenticationService, $qrCodeService, $errorLogService, $stateParams, $userCampaigns, $state, $userCampaignsRedeemFactory) ->
	$errorLogService.logMessage $stateParams

	$scope.globalSettings = angular.appSettings

	@projectBoldName = 'ZAPP'
	@projectName = 'DOBARA'
	@userName = 'Example user'
	@headerText = 'List off your redeem vouchers waiting'
	@descriptionText = 'Here you can quickly bootstrap your AngularJS project.'
	
	$scope.authenticated = $authenticationService.checkstatus()

	$scope.logout = ()->
		$authenticationService.logout()
		$scope.authenticated = false

	$scope.logoutClient = ()->
		$authenticationService.logoutClient()
		$scope.authenticated = false

	$scope.scanBarCode = ()->
		$errorLogService.logMessage $state.current
		
		# $qrCodeService.scanQRCodeTest($scope)
		# $userCampaigns.getData()
		$qrCodeService.scanQRCode($scope)
		$userCampaigns.getData()

		#$userCampaignsRedeemFactory.getRedeemData()

	return
