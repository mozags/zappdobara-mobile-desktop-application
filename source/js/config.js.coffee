config = ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise '/index'
  $stateProvider.state('index',
    url: '/index'
    templateUrl: 'views/index.html'
    data: pageTitle: 'index'
    )
    .state('login',
        url: '/login'
        templateUrl: 'views/login.html'
        controller: 'loginController'
        data: pageTitle: 'Login'
    )
    .state('sign-up',
        url: '/sign-up'
        templateUrl: 'views/sign-up.html'
        controller: 'registerController'
        data: pageTitle: 'Register'
    )
    .state('user-campaigns',
        url: '/user-campaigns'
        templateUrl: 'views/user-campaigns.html'
        controller: 'userCampaigns'
        data: pageTitle: 'User Campaigns'
    )
    .state('clientdashboard',
        url: '/client-dash-board'
        templateUrl: 'views/client-section/dashboard/index.html'
        controller: 'clientDashboardController'
        data: pageTitle: 'Client dashboard'
    )
    .state('formsbuilderlist',
        url: '/forms-list'
        templateUrl: 'views/admin/form-builder/index.html'
        controller: 'dataListController'
        data: pageTitle: 'Client dashboard'
    )
    .state('formsbuilderformnew',
        url: '/form-builder'
        templateUrl: 'views/admin/form-builder/form-builder.html'
        controller: 'formBuilderController'
        data: pageTitle: 'Client dashboard'
    )
    .state('formsbuilderformupdate',
        url: '/form-builder/:id'
        templateUrl: 'views/admin/form-builder/form-builder.html'
        controller: 'formBuilderController'
        data: pageTitle: 'Client dashboard'
    )
    # CAMPAIGN
    .state('campaignlist',
        url: '/campaign/list'
        templateUrl: 'views/global-views/list-view.html'
        controller: 'globalListController'
        data: pageTitle: 'Campaigns list'
    )
    .state('createcampaign',
        url: '/campaign'
        templateUrl: 'views/global-views/form-view.html'
        controller: 'crudController'
        data: pageTitle: 'Client dashboard'
    )
    .state('updatecampaign',
        url: '/campaign/:id'
        templateUrl: 'views/global-views/form-view.html'
        controller: 'crudController'
        data: pageTitle: 'Client dashboard'
    )
    # CAMPAIGN LOCATION
    .state('campaignlocationlist',
        url: '/campaign-location/list'
        templateUrl: 'views/global-views/list-view.html'
        controller: 'globalListController'
        data: pageTitle: 'Campaign location list'
    )
    .state('ceatecampaignlocation',
        url: '/campaign-location'
        templateUrl: 'views/global-views/form-view.html'
        controller: 'crudController'
        data: pageTitle: 'Client dashboard'
    )
    .state('updatecampaignlocation',
        url: '/campaign-location/:id'
        templateUrl: 'views/global-views/form-view.html'
        controller: 'crudController'
        data: pageTitle: 'Client dashboard'
    )
    # CAMPAIGN OWNER
    .state('ceatecampaignowner',
        url: '/campaign-owner'
        templateUrl: 'views/global-views/form-view.html'
        controller: 'crudController'
        data: pageTitle: 'Client dashboard'
    )
    .state('updatecampaignowner',
        url: '/campaign-owner/:id'
        templateUrl: 'views/global-views/form-view.html'
        controller: 'crudController'
        data: pageTitle: 'Client dashboard'
    )
    # QRCODE CAMPAIGN
     .state('campaignqrcode',
        url: '/campaign/qrcode/:id'
        templateUrl: 'views/qr/qr-view.html'
        controller: 'qrCodeController'
        data: pageTitle: 'campaign qrcode print'
    )
    #CLIENTS
    # CLIENTS LOGIN
    .state('loginClients',
        url: '/client/login'
        templateUrl: 'views/client-section/login.html'
        controller: 'loginController'
        data: pageTitle: 'Login'
    )
    .state('signUpClient',
        url: '/client/sign-up'
        templateUrl: 'views/client-section/sign-up.html'
        controller: 'registerController'
        data: pageTitle: 'Register'
    )
    # Get Campaign qrcode
    .state('campaignQRCODE',
        url: '/campaign/print/:id'
        templateUrl: 'views/client-section/print-campaign.html'
        controller: 'qrCodeController'
        data: pageTitle: 'Print'
    )
  return

angular.module('zappdobara').config(config).run ($rootScope, $state) ->
  $rootScope.$state = $state

  return