@zappdobara.controller "registerController", ($scope, $crudService, $errorLogService) ->
	$errorLogService.logMessage "Registration Controller"
	$scope.tplRegisterForm = "javascripts/app/templates/registerForm.html"

	$scope.register = (isValid)->
			$scope.register.username = $scope.register.email
			$scope.dataForServer =
				url: '/campaign-user'
				postdata: $scope.register
				type: 'POST'
			if isValid == true
				$scope.dataForServer.postdata.username = $scope.register.email
				$crudService.putPostService($scope, $scope.dataForServer, 'POST').then (data)->
					$errorLogService.logMessage data
					if data.id
						$errorLogService.logMessage "Success"
						$scope.add('You have now successfully registered', '')
						$scope.signedup = true
					else
						if data.httperror
							$errorLogService.logMessage "Failed"
							if angular.isDefined(data.httperror.dataReturned)
								if angular.isDefined(data.httperror.dataReturned.errors)
									if angular.isDefined(data.httperror.dataReturned.errors.username)
										msg = "Email #{data.httperror.dataReturned.errors.username}"
										$scope.add(msg, 'btn-danger')





	$scope.registerClient = (isValid)->
			$scope.register.username = $scope.register.email
			$scope.dataForServer =
				url: '/campaign-owner-user'
				postdata: $scope.register
				type: 'POST'
			if isValid == true
				$scope.dataForServer.postdata.username = $scope.register.email
				$crudService.putPostService($scope, $scope.dataForServer, 'POST').then (data)->
					$errorLogService.logMessage data
					if data.id
						$errorLogService.logMessage "Success"
						$scope.add('You have now successfully registered', '')
						$scope.signedup = true
					else
						if data.httperror
							$errorLogService.logMessage "Failed"
							if angular.isDefined(data.httperror.dataReturned)
								if angular.isDefined(data.httperror.dataReturned.errors)
									if angular.isDefined(data.httperror.dataReturned.errors.username)
										msg = "Email #{data.httperror.dataReturned.errors.username}"
										$scope.add(msg, 'btn-danger')

	###*
			 * Initialize index
			 * @type {number}
	###

	index = 0

	###*
	# Boolean to show error if new notification is invalid
	# @type {boolean}
	###

	$scope.invalidNotification = false

	###*
	# Placeholder for notifications
	#
	# We use a hash with auto incrementing key
	# so we can use "track by" in ng-repeat
	#
	# @type 
	###

	$scope.notifications = {}

	###*
	# Add a notification
	#
	# @param notification
	###

	$scope.add = (notification, classNotfication) ->
		i = undefined
		if !notification
			$scope.invalidNotification = true
			return
		i = index++
		$scope.invalidNotification = false
		$scope.notifications[i] = {}
		$scope.notifications[i].msg = notification
		$scope.notifications[i].msgClass = classNotfication

		return