@zappdobara.controller "pointsTestsController", ($scope, $errorLogService, $crudService, $authenticationService, $cordovaNetwork) ->
	
	$scope.apiUrlactive = angular.appSettings.urlServer

	$scope.scanTest = ()->
		arg = 
			url: "/campaign-user-scans"
			postdata:
				userId: angular.appSettings.userId
				campaignId: '45b585b04316ea18'
				active: 1
				archive: 0
			type: 'POST'							

		$crudService.putPostService($scope, arg, arg.type).then (data)->
			console.log data

	$scope.updateAPIUrl = (apiUrl)->
		angular.appSettings.urlServer = "http://#{apiUrl}:2403"
		$scope.apiUrlactive = "http://#{apiUrl}:2403"
