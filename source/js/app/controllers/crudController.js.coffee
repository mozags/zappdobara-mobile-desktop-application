@zappdobara.controller "crudController", ($scope, $errorLogService, $crudService, $authenticationService, $stateParams, $location, $q, $upload) ->
	#Get current url and split it 
	#Designed to match title of the form to pull
	formToGet = $location.url().split('/')

	$errorLogService.logMessage "Campaign Controller"
	$errorLogService.logMessage $location.url()
	
	$scope.myFormData = {}
	$scope.dataForServer = {}
	$scope.formcolection = {}

	#Global settings
	# $scope.formcolection.campaignOwnerId = angular.appSettings.campaignOwnerId
	$scope.formcolection.campaignOwnerUserId = angular.appSettings.campaignOwnerUserId
	$scope.formcolection.campaignOwnerId = angular.appSettings.campaignOwnerId
	
	$scope.pagetitle = formToGet[1]
	
	
	$errorLogService.logMessage formToGet
	
	
	$crudService.getHttp("/forms-collection?formTitle=#{formToGet[1]}").then (data) ->
		$scope.myForm = angular.fromJson(data[0].formSchema)

		if angular.isDefined($stateParams.id)
			$crudService.getHttp("/#{formToGet[1]}/#{$stateParams.id}").then (data) ->
				$scope.myForm = angular.fromJson(data.formSchema)
				$scope.formcolection = data
				$scope.dataForServer.url = "/#{formToGet[1]}/#{$stateParams.id}"
				$scope.dataForServer.type = 'PUT'
		else 
			$scope.dataForServer.type = 'POST'
			$scope.dataForServer.url = "/#{formToGet[1]}"


	$scope.submitForm = (isValid)->
		#Setup before we submit to server
		$errorLogService.logMessage isValid, 'is valid'
		
		$errorLogService.logMessage $scope.formcolection
		$scope.dataForServer.postdata = $scope.formcolection

		$errorLogService.logMessage	$scope.dataForServer
		if isValid
			$crudService.putPostService($scope, $scope.dataForServer, $scope.dataForServer.type).then (data)->	
				if angular.isDefined(data.id)
					$location.path("/#{formToGet[1]}/#{data.id}")
					if $scope.dataForServer.type == 'PUT' 
						$scope.add("Sucessfully updated #{formToGet[1]}", '')
					if $scope.dataForServer.type == 'POST'
						$scope.add("Sucessfully Created #{formToGet[1]}", '')

				$errorLogService.logMessage data.id
				$errorLogService.logMessage "Request type: #{$scope.dataForServer.type}"
		else
			$scope.add("You have errors", 'alert-danger')

	###*
			 * Initialize index
			 * @type {number}
	###

	index = 0

	###*
	# Boolean to show error if new notification is invalid
	# @type {boolean}
	###

	$scope.invalidNotification = false

	###*
	# Placeholder for notifications
	#
	# We use a hash with auto incrementing key
	# so we can use "track by" in ng-repeat
	#
	# @type 
	###

	$scope.notifications = {}

	###*
	# Add a notification
	#
	# @param notification
	###

	$scope.add = (notification, classNotfication) ->
		i = undefined
		if !notification
			$scope.invalidNotification = true
			return
		i = index++
		$scope.invalidNotification = false
		$scope.notifications[i] = {}
		$scope.notifications[i].msg = notification
		$scope.notifications[i].msgClass = classNotfication

		return