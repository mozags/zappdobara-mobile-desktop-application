@zappdobara.controller "navigationController", ($scope, $qrCodeService, $authenticationService, $location) ->
	console.log "Navigation Controller"

	$scope.globalSettings = angular.appSettings

	$scope.scanBarCode = ()->
		$qrCodeService.scanQRCode($scope)

	$scope.logout = ()->
		$authenticationService.logout()
		$location.path("/login")

