@zappdobara.controller "qrCodeController", ($scope, $crudService, $errorLogService, $stateParams) ->
	$errorLogService.logMessage "qrCode Controller"
	if angular.isDefined($stateParams.id)
		$scope.qrCode = $stateParams.id
		$crudService.getHttp("/campaign/#{$stateParams.id}").then (data)->
			if data.id
				$scope.campaign = data
			
