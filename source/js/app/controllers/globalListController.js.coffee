@zappdobara.controller "globalListController", ($scope, $errorLogService, $crudService, $authenticationService, $stateParams, $location, $q, $upload, $modal, $modalStack) ->
	#Get current url and split it 
	#Designed to match title of the form to pull
	formToGet = $location.url().split('/')

	$errorLogService.logMessage "globalListController"
	$errorLogService.logMessage $location.url()
	#Global settings
	# $scope.campaignOwnerId = "aea170d3a598682f"
	$scope.campaignOwnerUserId = angular.appSettings.campaignOwnerUserId
	$scope.tplView = "views/global-views/templates-data-tables-views/#{formToGet[1]}.html"
	$scope.pagetitle = formToGet[1]
	
	$errorLogService.logMessage formToGet
	
	$scope.getArguments =
		campaignOwnerUserId: angular.appSettings.campaignOwnerUserId
		
	$crudService.getHttpWithParams("/#{formToGet[1]}", $scope.getArguments).then (data) ->
		$errorLogService.logMessage data
		$scope.listData = data
	
	$scope.detailListItem = (id)->
		modalInstance = $modal.open(
			templateUrl:  "views/global-views/template-modal-pop-up/#{formToGet[1]}.html"
			controller: "viewListController"
			resolve:
				items: ()->
					return id
			)
		return

	$scope.deleteItem = (id)->
		modalInstance = $modal.open(
			templateUrl:  "views/global-views/template-modal-pop-up/confirm.html"
			controller: "deleteItemController"
			resolve:
				items: ()->
					return id
			)
		return

@zappdobara.controller "viewListController", ($scope, $errorLogService, $crudService, $authenticationService, $stateParams, $location, $modalInstance, items) ->
	#Get current url and split it 
	#Designed to match title of the form to pull
	$scope.formToGet = $location.url().split('/')
	$scope.items = items
	$crudService.getHttp("/#{$scope.formToGet[1]}/#{items}").then (data) ->
		$errorLogService.logMessage data
		$scope.viewData = data

	$scope.cancel = ()->
		$modalInstance.close()

@zappdobara.controller "deleteItemController", ($scope, $errorLogService, $crudService, $authenticationService, $stateParams, $location, $modalInstance, items, $route, $window) ->
	#Get current url and split it 
	#Designed to match title of the form to pull
	$scope.currenturl = $location.url()
	$scope.formToGet = $location.url().split('/')
	$scope.dataForServer = {}
	$scope.items = items

	$scope.cancel = ()->
		$modalInstance.close()

	$scope.deleteItem = ()->
		$scope.dataForServer.postdata = {}
		$scope.dataForServer.type = "DELETE"
		$scope.dataForServer.url = "/#{$scope.formToGet[1]}/#{items}"
		$errorLogService.logMessage	$scope.dataForServer

		$crudService.putPostService($scope, $scope.dataForServer, $scope.dataForServer.type).then (data)->	

			$window.location.reload()
			$errorLogService.logMessage data.id
			$errorLogService.logMessage "Request type: #{$scope.dataForServer.type}"
			$scope.cancel()