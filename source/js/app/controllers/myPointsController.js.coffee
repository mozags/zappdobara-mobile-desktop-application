@zappdobara.controller "myPointsController", ($scope, $errorLogService, $crudService, $authenticationService, $userCampaignsRedeemFactory) ->
	$errorLogService.logMessage 'myPointsController'
	$scope.redeemCampaign = $userCampaignsRedeemFactory

	$scope.init = ()->
		$scope.redeemCampaign.getRedeemData()

	$scope.redeemVocher = (redeemId)->
		$errorLogService.logMessage redeemId


		swal {
			title: 'Redeem your vocher?'
			text: ''
			type: 'warning'
			showCancelButton: true
			confirmButtonColor: '#DD6B55'
			confirmButtonText: 'Yes'
			cancelButtonText: 'No'
			closeOnConfirm: true
			closeOnCancel: true
		}, (isConfirm) ->
			if isConfirm
				$scope.redeemCampaign.redeemVoucher($scope, redeemId)
				swal 'Congratulations!', 'You have redeemed your voucher.', 'success'
			else
				#swal 'Cancelled', '', 'error'
			return

	$scope.init()
