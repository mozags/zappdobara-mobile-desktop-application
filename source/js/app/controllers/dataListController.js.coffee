@zappdobara.controller "dataListController", ($scope, $errorLogService, $crudService, $authenticationService, $stateParams, $location, $q, $upload, $timeout) ->
	#Get current url and split it 
	#Designed to match title of the form to pull
	formToGet = $location.url().split('/')

	$errorLogService.logMessage "Campaign Controller"
	$errorLogService.logMessage $location.url()

	$scope.listData = {}
	
	$scope.pagetitle = formToGet[1]
	
	
	$errorLogService.logMessage formToGet
	
	
	$crudService.getHttp("/forms-collection").then (data) ->

		$errorLogService.logMessage data
		$scope.listData = data
		