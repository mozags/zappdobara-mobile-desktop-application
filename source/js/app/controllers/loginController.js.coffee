@zappdobara.controller "loginController", ($scope, $errorLogService, $crudService, $authenticationService, $cordovaNetwork) ->
	$scope.globalSetting = angular.appSettings.urlServer
	$scope.tplLoginForm = "javascripts/app/templates/loginForm.html"
	$scope.login = {}

	$scope.loginMessages = []
	$scope.showErrors = false


	$scope.checkNetworkStatus = ()->
		document.addEventListener "deviceready", ()->
			alert($cordovaNetwork.isOffline())

	###*
	# Login user by verifying user credentials server
	* @method $scope.loginFormSubmit
	* @param {string} formStatus "Form status ie form is widthout errors or not (true / false)"
	* @return null
	###


	$scope.loginFormSubmit = (formStatus)->

		$errorLogService.logMessage "Submit initiated"
		$errorLogService.logMessage formStatus

		$scope.dataForServer =
			url: '/campaign-user/login'
			postdata: $scope.login
			type: 'POST'

		if formStatus == true
			#if $cordovaNetwork.isOnline()
			$scope.showErrors = false
			$scope.message = 'Form is valid!'
			$authenticationService.login($scope, $scope.dataForServer)
			$errorLogService.logMessage $authenticationService.checkstatus()
			#else
			#	$scope.formSubmissionFailedMessage = 'Please check you network connection'
		else
			$scope.message = 'Please correct these errors:'

		return



	###*
	# Login client user by verifying user credentials server
	* @method $scope.loginFormSubmit
	* @param {string} formStatus "Form status ie form is widthout errors or not (true / false)"
	* @return null
	###


	$scope.loginFormSubmitClients = (formStatus)->

		$errorLogService.logMessage "Submit initiated"
		$errorLogService.logMessage formStatus

		$scope.dataForServer =
			url: '/campaign-owner-user/login'
			postdata: $scope.login
			type: 'POST'

		if formStatus == true
			#if $cordovaNetwork.isOnline()
			$scope.showErrors = false
			$scope.message = 'Form is valid!'
			$authenticationService.loginClients($scope, $scope.dataForServer)
			$errorLogService.logMessage $authenticationService.checkstatus()
			#else
			#	$scope.formSubmissionFailedMessage = 'Please check you network connection'
		else
			$scope.message = 'Please correct these errors:'

		return
	$scope.changeUrlServer = (changeUrlServer)->
		$errorLogService.logMessage changeUrlServer
		if changeUrlServer != 'false'
			angular.appSettings.urlServer = changeUrlServer
		else
			angular.appSettings.urlServer = $scope.changeUrlServerInput

		$scope.globalSetting = angular.appSettings.urlServer
