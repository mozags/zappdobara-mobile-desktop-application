@zappdobara.controller "formCreationController", ($scope, $errorLogService, $crudService) ->
	$errorLogService.logMessage "formCreationController"
	
	$scope.myForm = {}

	$scope.submitForm = ()->
		$errorLogService.logMessage "Submit initiated"
		$scope.dataForServer =
			url: '/forms-collection'
			postdata:
				#Convert to JSON string 
				formJson: angular.toJson($scope.myForm)
			type: 'POST'

		$errorLogService.logMessage	 $scope.dataForServer

		$crudService.putPostService($scope, $scope.dataForServer, $scope.dataForServer.type).then (data)->	

			$errorLogService.logMessage "After submit request"
			$errorLogService.logMessage "Request type: #{$scope.dataForServer.type}"
			$errorLogService.logMessage data
