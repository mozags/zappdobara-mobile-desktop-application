@zappdobara.controller "formListController", ($scope, $errorLogService, $crudService) ->
	$errorLogService.logMessage "formListCreationController"

	$scope.init = ()->
		$scope.dependancies()
	$scope.dependancies = ()->
		$crudService.getHttp('/forms-collection').then (formData)->
			$errorLogService.logMessage formData

	$scope.init()	