@zappdobara.controller "clientDashboardController", ($scope, $errorLogService, $crudService, $authenticationService, $stateParams, $location, $q, $upload) ->
	$errorLogService.logMessage "clientDashboardController"
	# Globals
	$scope.clientcampaignUsersCount = 0
	$scope.campaignCount = 0
	$scope.clinetUsersAwatingRedeem = []
	$scope.clientCampaigns = []

	$scope.init = ()->
		$scope.getUserData()

	$scope.getUserData = ()->
		userData = $authenticationService.getuser()
		$crudService.getHttp("/campaign-owner-user/#{userData.pointssttaus}").then (data)->
			$errorLogService.logMessage data, "Cleint user data"
			$scope.ownerId = data.campaignOwnerId
			$scope.getCampaigns()
			$scope.countUsers()

	$scope.getCampaigns = ()->
		#Count of how many campaigns client has
		query = {}
		query.campaignOwnerId = $scope.ownerId
		query.dashboardcampaigns = true

		$crudService.getHttpWithParams("/campaign", query).then (data)->
			$scope.campaignCount = data.length
			$scope.clientCampaigns = data
			$errorLogService.logMessage data

	$scope.countUsers = ()->
		#Count of how many users on thier campaigns
		query = {}
		query.campaignOwnerId = $scope.ownerId
		$crudService.getHttpWithParams("/campaign-user-campaigns", query).then (data)->
			$scope.clientcampaignUsersCount = data.length	

	$scope.awaitingUsersToRedeem = (campaignId)->
		#Users campaigns awaiting free redeem.
		query = {}
		query.campaignId = campaignId

		d = $q.defer()
		$crudService.getHttpWithParams("/redeem-campaign", query).then (data)->
			$errorLogService.logMessage data, "Redeem for campaign"
			d.resolve data
		return d.promise
	$scope.init()