#Service for authenticationService
@zappdobara.service '$authenticationService', ($http, $filter, $errorLogService, $rootScope, $crudService, $cookieStore, $location) ->
	authenticationService = {}

	authenticationService.login = ($scope, userCredentials)->
		$errorLogService.logMessage 'Ready to login'
		$crudService.putPostService($scope, userCredentials, 'POST').then (data)->
			status = false
			if data.uid
				$crudService.getHttp("/campaign-user/#{data.uid}").then (meData)->
					userdata = {}
					userdata.pointssttaus = data.uid
					userdata.pointstype = 'usr' # user
					if angular.isDefined(window.localStorage)

						window.localStorage.setItem("fgyhbjlloljnhngfdvsyvdy", angular.toJson(userdata))
						@angular.appSettings.userId = data.uid
						$errorLogService.logMessage angular.appSettings
						status = true
					#Parent controller user loged in
					$scope.$parent.authenticated = true

					$location.path("/index")
			
			else
				$scope.formSubmissionFailedMessage = "Your credentials did not match please try again"

		return status


	authenticationService.loginClients = ($scope, userCredentials)->
		$errorLogService.logMessage 'Ready to login'
		$crudService.putPostService($scope, userCredentials, 'POST').then (data)->
			status = false
			if data.uid
				$crudService.getHttp("/campaign-owner-user/#{data.uid}").then (meData)->
					userdata = {}
					userdata.pointssttaus = data.uid
					userdata.ownstatus = meData.campaignOwnerId
					userdata.pointstype = 'cli' # client
					if angular.isDefined(window.localStorage)

						window.localStorage.setItem("fgyhbjlloljnhngfdvsyvdy", angular.toJson(userdata))
						@angular.appSettings.campaignOwnerUserId= data.uid
						@angular.appSettings.campaignOwnerId= data.campaignOwnerId

						$errorLogService.logMessage angular.appSettings
						status = true
					#Parent controller user loged in
					$scope.$parent.authenticated = true

					$location.path("/client-dash-board")
			
			else
				$scope.formSubmissionFailedMessage = "Your credentials did not match please try again"

		return status

	authenticationService.checkstatus = ()->
		statusUser = false
		$errorLogService.logMessage window.localStorage.getItem("fgyhbjlloljnhngfdvsyvdy")

		if window.localStorage.getItem("fgyhbjlloljnhngfdvsyvdy")
			statusUser = true

		return statusUser

	authenticationService.getuser = ()->
		statusUser = false
		$errorLogService.logMessage window.localStorage.getItem("fgyhbjlloljnhngfdvsyvdy")

		if window.localStorage.getItem("fgyhbjlloljnhngfdvsyvdy")
			statusUser = angular.fromJson(window.localStorage.getItem("fgyhbjlloljnhngfdvsyvdy"))

		return statusUser

	authenticationService.logout = ()->
		if angular.isDefined(window.localStorage)
			if window.localStorage.getItem("fgyhbjlloljnhngfdvsyvdy")
				window.localStorage.removeItem("fgyhbjlloljnhngfdvsyvdy")
				$errorLogService.logMessage 'USER LOGGEDOUT'
				$location.path("/login")

	authenticationService.logoutClient = ()->
		if angular.isDefined(window.localStorage)
			if window.localStorage.getItem("fgyhbjlloljnhngfdvsyvdy")
				window.localStorage.removeItem("fgyhbjlloljnhngfdvsyvdy")
				$errorLogService.logMessage 'USER LOGGEDOUT'
				$location.path("/client/login")

	return authenticationService
