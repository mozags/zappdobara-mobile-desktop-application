@zappdobara.service '$fileuploadservice', ($http, $filter, $log, $timeout, $helperservice, $infoalerts, $upload, $q, $ajaxrequestsvr) ->
	fileuploadservice = {}
	fileuploadservice.setup = ($scope)->
		$scope.uploadurls =
			identity: '/api/v2/upload_identity'

		$scope.globalfilestoupload = []


	#Documents uploaded in a gloabl $scope var list
	fileuploadservice.sucessfulluploadsDocuments = ($scope, file_nameuploaded) ->

		if angular.isDefined($scope.filesuploadedsucessdocuments)
			$scope.filesuploadedsucessdocuments.push({file_name:file_nameuploaded})
		else
			$scope.filesuploadedsucessdocuments = []
			$scope.filesuploadedsucessdocuments.push({file_name:file_nameuploaded})

	#Assets (Images) uploaded in a gloabl $scope var list
	fileuploadservice.sucessfulluploadsAssets = ($scope, file_nameuploaded) ->

		if angular.isDefined($scope.filesuploadedsucessassets)
			$scope.filesuploadedsucessassets.push({file_name:file_nameuploaded})
		else
			$scope.filesuploadedsucessassets = []
			$scope.filesuploadedsucessassets.push({file_name:file_nameuploaded})

	#Temp files (Images, Documents) uploaded in a gloabl $scope var list used when
	#user creates a new record to the database which has not got a id for use,
	fileuploadservice.sucessfulluploadsTmp = ($scope, file_nameuploaded, file_nameuploaded_type) ->

		if angular.isDefined($scope.filesuploadedsucesstmp)
			$scope.filesuploadedsucesstmp.push({file_name:file_nameuploaded, file_type:file_nameuploaded_type})
		else
			$scope.filesuploadedsucesstmp = []
			$scope.filesuploadedsucesstmp.push({file_name:file_nameuploaded, file_type:file_nameuploaded_type})

	#Clear all list of files uploaded
	fileuploadservice.clearuploadedfilemesg = ($scope)->

		if angular.isDefined($scope.filesuploadedsucessassets)
			delete  $scope.filesuploadedsucessassets = ''

		if angular.isDefined($scope.filesuploadedsucessdocuments)
			delete  $scope.filesuploadedsucessdocuments = ''

		if angular.isDefined($scope.filesuploadedsucesstmp)
			delete $scope.filesuploadedsucesstmp = ''

		if angular.isDefined($scope.fileuploadmsg)
			delete $scope.fileuploadmsg = ''

		if angular.isDefined($scope.globalfilestoupload)
			$scope.globalfilestoupload = []

	fileuploadservice.testfileextdocs = (filename, $scope)->

		returnstatus = false
		file_ext = []
		file_ext[0] = 'pdf'
		file_ext[1] = 'doc'
		file_ext[2] = 'docx'

		filext = filename.split('.')
		exttocompare = filext[filext.length-1]

		for i in file_ext
			console.log exttocompare == i
			if exttocompare == i
				returnstatus = true
				$scope.fileuploadmsg = ''

		console.log returnstatus
		if returnstatus
			$scope.fileuploadmsg = ''
		# else
		# 	$scope.fileuploadmsg = 'File not permitted to be uploaded'

		return returnstatus

	fileuploadservice.testfileextimg = (filename, $scope)->
		console.log 'IMG'
		returnstatus = false
		file_ext = []
		file_ext[0] = 'png'
		file_ext[1] = 'jpeg'
		file_ext[2] = 'gif'
		file_ext[3] = 'jpg'

		filext = filename.split('.')
		exttocompare = filext[filext.length-1]

		for i in file_ext
			console.log exttocompare == i
			if exttocompare == i
				returnstatus = true
				$scope.fileuploadmsg = ''

		console.log returnstatus

		if returnstatus
			$scope.fileuploadmsg = ''
		# else
		# 	$scope.fileuploadmsg = 'File not permitted to be uploaded'

		return returnstatus

	fileuploadservice.addtofilesarrayforfilesupload = ($files, $scope)->

		$scope.globalfilestoupload.push($files)

	fileuploadservice.uploadfilesfromfilesarray = ($scope, urltoupload)->
		for i in $scope.globalfilestoupload
			console.log i
			$scope.uploadfilesettings.newsassets.size = i[0].size
			fileuploadservice.onFileSelectUpload(i, $scope, urltoupload)


	fileuploadservice.onFileSelectUpload = ($files, $scope, upload_url) ->
		d = $q.defer()

		fileuploadservice.uploadtoserver($files, $scope, upload_url).then (data)->
			d.resolve data

		return d.promise


	fileuploadservice.uploadtoserver = ($files, $scope, serverurl)->
		console.log $files
		i = 0
		file = $files[i]
		console.log $scope.filedata
		d = $q.defer()
		$scope.upload = $upload.upload(
			url: serverurl
			headers:
				'Content-Type': false
			fields: 
				myObj: $scope.filedata
			data: {}
			file: file
			sendObjectsAsJsonBlob: false
		).progress((evt) ->
			console.log "percent: " + parseInt(100.0 * evt.loaded / evt.total)
			return
		).success((data, status, headers, config) ->
			d.resolve data
			# file is uploaded successfully
			return
		)

		return d.promise

	return fileuploadservice
