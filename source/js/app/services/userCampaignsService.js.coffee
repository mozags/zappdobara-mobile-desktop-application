@zappdobara.factory "$userCampaigns", ($errorLogService, $crudService, $authenticationService) ->
	userCampaigns = {}
	userCampaigns.campaignData = {}
	#/*******************
	# Function name: userCampaigns.getData
	# Arguments: nil
	# Description: gets alist of the users current campaigns
	#*******************/
	userCampaigns.getData = ()->
		#Setup query
		userData = $authenticationService.getuser()

		query = {}
		query.userId = userData.pointssttaus

		$crudService.getHttpWithParams("/campaign-user-campaigns", query).then (data)->
			$errorLogService.logMessage data
			if data.length > 0
				userCampaigns.campaignData = data
			else
				userCampaigns.campaignData = true

	return userCampaigns
