@zappdobara.service '$qrCodeService', ($http, $crudService, $errorLogService, $window, $q) ->
	qrCodeService = {}
	qrCodeService.setup = ()->

	qrCodeService.scanQRCode = ($scope)->
		document.addEventListener "deviceready", ()->
			if angular.isDefined(cordova)
				if angular.isDefined(cordova.plugins)
					if angular.isDefined(cordova.plugins.barcodeScanner)
						cordova.plugins.barcodeScanner.scan ((result) ->
							#alert "We got a barcode\n" + "Result: " + result.text + "\n" + "Format: " + result.format + "\n" + "Cancelled: " + result.cancelled
							$scope.qrCodeResult = result.text
							arg =
								url: "/campaign-user-scans"
								postdata:
									userId: angular.appSettings.userId
									campaignId: $scope.qrCodeResult
									active: 1
									archive: 0
								type: 'POST'
							if $scope.qrCodeResult.length > 0 
								$crudService.putPostService($scope, arg, arg.type).then (data)->
									if angular.isDefined(data.id)
										swal
											title: 'Your point has been added'
											text: "Success!"
											type: 'success'
											confirmButtonText: 'OK'
									else
										swal
											title: 'Your point has been added and will update once we instablish network connection'
											text: "Success!"
											type: 'success'
											confirmButtonText: 'OK'
										qrCodeService.saveLocal($scope.qrCodeResult)
							return
						), (error) ->
							#alert "Scanning failed: " + error
							$scope.qrCodeResult = error
							swal
								title: 'SCAN Result!'
								text: "There was a error scanning your campaign code!"
								type: 'error'
								confirmButtonText: 'OK'

							return

	qrCodeService.scanQRCodeTest = ($scope)->
		
		$scope.qrCodeResult = '95e00b1ad72558a7';
		arg =
			url: "/campaign-user-scans"
			postdata:
				userId: angular.appSettings.userId
				campaignId: $scope.qrCodeResult
				active: 1
				archive: 0
			type: 'POST'

		$crudService.putPostService($scope, arg, arg.type).then (data)->
			if angular.isDefined(data.id)
				swal
					title: 'Your point has been added'
					text: "Success!"
					type: 'success'
					confirmButtonText: 'OK'
			else
				swal
					title: 'Your point has been added and will update once we instablish network connection'
					text: "Success!"
					type: 'success'
					confirmButtonText: 'OK'
					
				qrCodeService.saveLocal($scope.qrCodeResult)

	qrCodeService.saveLocal = (qrCodeCampaignId)->
		if angular.isDefined(window.localStorage)
			if window.localStorage.getItem("savedScans")
				saveddata = angular.fromJson(window.localStorage.getItem("savedScans"))
				saveddata.push(qrCodeCampaignId)
				window.localStorage.setItem("savedScans", angular.toJson(saveddata))
			else
				saveddata = []
				saveddata.push(qrCodeCampaignId)
				window.localStorage.setItem("savedScans", angular.toJson(saveddata))

	qrCodeService.updateSavedScans = ($scope)->
		if window.localStorage.getItem("savedScans")
			saveddata = angular.fromJson(window.localStorage.getItem("savedScans"))	
			notsavedData = []

			for campId in saveddata
				$errorLogService.logMessage campId, 'Saved data update'

				arg =
					url: "/campaign-user-scans"
					postdata:
						userId: angular.appSettings.userId
						campaignId: campId
						active: 1
						archive: 0
					type: 'POST'

				$crudService.putPostService($scope, arg, arg.type).then (data)->
					$errorLogService.logMessage angular.isUndefined(data.id), 'data from server'
					if angular.isUndefined(data.id)
						console.log notsavedData
						notsavedData.push(campId)

			$errorLogService.logMessage notsavedData, 'No saved array'

			if notsavedData.length > 0
				window.localStorage.setItem("savedScans", angular.toJson(notsavedData))
			else
				window.localStorage.removeItem("savedScans")	

	return	qrCodeService
