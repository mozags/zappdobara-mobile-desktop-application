@zappdobara.factory "$userCampaignsRedeemFactory", ($errorLogService, $crudService, $authenticationService)->

  userCampaignsRedeemFactory = {}

  userCampaignsRedeemFactory.redeemData = {}

  #Get campaigns for this user
  userCampaignsRedeemFactory.getRedeemData = ()->

    #Setup query
    userData = $authenticationService.getuser()

    query = {}
    query.userId = userData.pointssttaus
    $crudService.getHttpWithParams("/campaign-user-campaigns", query).then (data)->
      if data.length > 0
        userCampaignsRedeemFactory.redeemData = data
      else
        userCampaignsRedeemFactory.redeemData = true
        
  #Get campaigns for this user
  userCampaignsRedeemFactory.redeemVoucher = ($scope,voucherId)->

    #Setup query
    arg =
      url: "/redeem-campaign/#{voucherId}"
      postdata:
        userId: angular.appSettings.userId
        active: 0
        archive: 1
      type: 'PUT'

    $crudService.putPostService($scope, arg, arg.type).then (data)->
      $errorLogService.logMessage data, 'Data redeem-campaign returned'
      userCampaignsRedeemFactory.getRedeemData()

    
    return true

  return userCampaignsRedeemFactory
