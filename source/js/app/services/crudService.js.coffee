#Service for CRUD services
@zappdobara.service '$crudService', ($http, $filter, $log, $q) ->
	crudService = {}

	###
	Used to POST / PUT request to the server

	@method putPostService
	@param {Object} arg object of options
	@param {object} arg.url URL of post/put
	@param {object} arg.postdata Object of data to pass
	@param {string} Type of request "PUT or POST"
	@return {Object} Returns object error or sucess object
	###

	crudService.putPostService = ($scope, arg, type)->
		d = $q.defer()

		$http(
			method: type
			url: angular.appSettings.urlServer+arg.url
			data: $.param(arg.postdata)
			headers: "Content-Type": "application/x-www-form-urlencoded"
			cache: false
		).success((data, status, headers, config) ->

			d.resolve data

		).error (data, status, headers, config) ->
			# console.log typeof status
			globalhttperror = {}
			globalhttperror.httperror = {}
			if status == 422
				globalhttperror.httperror.message = 'Unfortunatly there was a error please try again'
				globalhttperror.httperror.dataReturned = data
			else
				globalhttperror.httperror.message = 'Unfortunatly there was a error please try again'
				globalhttperror.httperror.dataReturned = data
			globalhttperror.httperror.status = status

			d.resolve globalhttperror

		return d.promise

	###
	Used to GET request from server
	@method putPostService
	@param {string} urlToGet api URL
	###
	crudService.getHttp = (urlToGet) ->
		urltoget = "#{angular.appSettings.urlServer}#{urlToGet}"

		d = $q.defer()
		$http.get(urltoget).success (data) ->
			d.resolve data
		# console.log d.promise
		return d.promise

	###
	Used to GET request from server with params
	@method putPostService
	@param {string} urlToGet api URL
	@param {object} parameters json object of parameters
	###

	crudService.getHttpWithParams = (urltoget, parameters) ->
		urltoget = "#{angular.appSettings.urlServer}#{urltoget}"
		d = $q.defer()
		$http.get(urltoget,
			params: parameters
		).success (data) ->
			d.resolve data
		return d.promise

	return crudService
