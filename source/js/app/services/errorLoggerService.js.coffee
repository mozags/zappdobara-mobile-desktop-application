#Service for CRUD services
@zappdobara.service '$errorLogService', ($http, $filter, $log, $q) ->
	errorLogService = {}
	errorLogService.debug = true
	errorLogService.logMessage = (message, debugmessage, linenumber, controller, $scope)->

		if angular.isDefined(debugmessage) && errorLogService.debug is true
			console.log "Debug Message: #{debugmessage}"

		if angular.isDefined(linenumber) && errorLogService.debug is true
			console.log "Debug Linenumber: #{linenumber}"

		if angular.isDefined(message) && errorLogService.debug is true
			console.log message

		if angular.isDefined(controller) && errorLogService.debug is true
			console.log "Debug Controller name: #{controller}"

		if angular.isDefined(debugmessage) && errorLogService.debug is true
			console.log "End Debug Message: #{debugmessage}"

	return errorLogService
