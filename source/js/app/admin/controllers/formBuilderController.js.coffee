@zappdobara.controller "formBuilderController", ($scope, $errorLogService, $crudService, $authenticationService, $stateParams, $location) ->
	$errorLogService.logMessage "Campaign Controller"
	$errorLogService.logMessage $stateParams
	# Global vars settings
	$scope.myForm = {}
	$scope.dataForServer = {}
	$scope.formcolection = {}

	if angular.isDefined($stateParams.id)
		$crudService.getHttp("/forms-collection/#{$stateParams.id}").then (data) ->
			$scope.myForm = angular.fromJson(data.formSchema)
			$scope.formcolection = data
			$errorLogService.logMessage $scope.myForm, 'booo'
			$scope.dataForServer.url = "/forms-collection/#{$stateParams.id}"
			$scope.dataForServer.type = 'PUT'
	else 
		$scope.dataForServer.type = 'POST'
		$scope.dataForServer.url = "/forms-collection"
	

	$scope.submitForm = ()->
		#Setup before we submit to server
		$errorLogService.logMessage "Submit initiated"
		$scope.formcolection.formSchema = angular.toJson($scope.myForm)
		$errorLogService.logMessage $scope.formcolection
		$scope.dataForServer.postdata = $scope.formcolection
		$errorLogService.logMessage	$scope.dataForServer

		$crudService.putPostService($scope, $scope.dataForServer, $scope.dataForServer.type).then (data)->	
			if angular.isDefined(data.id)
				$location.path("/form-builder/#{data.id}")
			$errorLogService.logMessage data.id
			$errorLogService.logMessage "Request type: #{$scope.dataForServer.type}"
